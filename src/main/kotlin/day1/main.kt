package day1

import java.io.File

fun part1(filePath: String) {
    var freq = 0

    File(filePath).forEachLine {
        freq += it.toInt()
    }

    println(freq)
}

fun part2(filePath: String) {
    var freq = 0
    val freqs = mutableSetOf<Int>()

    val lines = File(filePath).readLines()

    var index = 0
    while (true) {
        freq += lines[index++ % lines.size].toInt()

        if (freqs.contains(freq)) {
            break
        }

        freqs.add(freq)
    }

    println(freq)
}

fun main(args: Array<String>) {
    val path = ClassLoader.getSystemResource("day1/input.txt").path
    part1(path)
    part2(path)
}